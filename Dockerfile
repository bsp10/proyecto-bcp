# Base Alpine Linux based image with OpenJDK JRE only
FROM openjdk:8-jdk-slim
LABEL maintainer="image-BCP-v1"
COPY "./target/bcp-0.0.1-SNAPSHOT.jar" "app.jar"
ENTRYPOINT ["java", "-jar", "app.jar"]
EXPOSE 8080