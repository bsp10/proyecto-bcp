package pe.com.bcp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pe.com.bcp.model.RespuestaErr;
import pe.com.bcp.model.RespuestaOK;
import pe.com.bcp.model.TipoCambioModel;
import pe.com.bcp.service.TipoCambioService;
import rx.Single;
import rx.schedulers.Schedulers;

@RestController
@RequestMapping("/bcp/tipoCambio")
public class TipoCambioController {
	
	@Autowired
	private TipoCambioService tipoCambioService;
	
	@PostMapping("/registrar")
	public Single<ResponseEntity<RespuestaOK>> save(@RequestBody TipoCambioModel tipoCambio){
		return tipoCambioService.save(tipoCambio)
							.subscribeOn(Schedulers.io())
							.map(x -> ResponseEntity.ok(new RespuestaOK("Registro insertado")))
							.onErrorReturn(y -> {
								RespuestaErr err = new RespuestaErr("001", "ERROR " + y.toString());
								return new ResponseEntity(err, HttpStatus.INTERNAL_SERVER_ERROR);
							});
	}
	
	@PostMapping("/actualizar")
	public Single<ResponseEntity<RespuestaOK>> update(@RequestBody TipoCambioModel tipoCambio){
		return tipoCambioService.actualizar(tipoCambio)
							.subscribeOn(Schedulers.io())
							.map(x -> ResponseEntity.ok(new RespuestaOK("Registro actualizado")))
							.onErrorReturn(y -> {
								System.out.println(y);
								RespuestaErr err = new RespuestaErr("001", "ERROR " + y.getMessage());
								return new ResponseEntity(err, HttpStatus.INTERNAL_SERVER_ERROR);
							});
	}	
	
	@GetMapping("/consultar")
	public Single<ResponseEntity<TipoCambioModel>> consultarCambio(@RequestParam(required = true) String monOrigen, @RequestParam(required = true) String monDestino, @RequestParam(required = true) Double montoAntesCambio) {
		System.out.println("montoAntesCambio: " + montoAntesCambio);
		return tipoCambioService.consultarCambio(monOrigen, monDestino, montoAntesCambio)
								.subscribeOn(Schedulers.io())
								.map(t -> ResponseEntity.ok(t))
								.onErrorReturn(y -> {
									System.out.println(y);
									RespuestaErr respuestaErr = new RespuestaErr("001", "Ocurrió un Error");
									return new ResponseEntity(respuestaErr, HttpStatus.INTERNAL_SERVER_ERROR);
								});
	}
}
