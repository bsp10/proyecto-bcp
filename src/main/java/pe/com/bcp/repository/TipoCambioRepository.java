package pe.com.bcp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import pe.com.bcp.entity.TipoCambioEntity;

public interface TipoCambioRepository extends JpaRepository<TipoCambioEntity, Long> {
    
	@Transactional
	@Modifying
    @Query(value = "UPDATE tipo_cambio SET montotcambio = ?3, usuario_act = ?4, fecha_act = ?5 WHERE mon_origen = ?1 AND mon_destino = ?2", nativeQuery = true)
    void actualizar(String monOrigen, String monDestino, Double montoTCambio, String usuarioAct, String fechaAct);
    
    @Query(value = "SELECT * FROM tipo_cambio WHERE mon_origen = ?1 AND mon_destino = ?2", nativeQuery = true)
	List<TipoCambioEntity> consultarCambio(String monOrigen,String monDestino);

}
