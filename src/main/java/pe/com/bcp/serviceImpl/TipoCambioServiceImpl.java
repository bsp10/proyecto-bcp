package pe.com.bcp.serviceImpl;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.reactivex.Flowable;
import pe.com.bcp.entity.TipoCambioEntity;
import pe.com.bcp.model.TipoCambioModel;
import pe.com.bcp.repository.TipoCambioRepository;
import pe.com.bcp.service.TipoCambioService;
import rx.Single;

@Service
public class TipoCambioServiceImpl implements TipoCambioService {
	
	@Autowired
	private TipoCambioRepository tipoCambioRepository;
	
	@Override
	public Single<Void> save(TipoCambioModel tipoCambio) {
		LocalDate todaysDate = LocalDate.now();
		// List<TipoCambioEntity> list = tipoCambioRepository.findAll();
		TipoCambioEntity tipoCambioEntity = new TipoCambioEntity();
		tipoCambioEntity.setMonOrigen(tipoCambio.getMonOrigen());
		tipoCambioEntity.setMonDestino(tipoCambio.getMonDestino());
		tipoCambioEntity.setMontoTCambio(tipoCambio.getMontoTCambio());
		tipoCambioEntity.setUsuarioCrea(System.getenv("USERNAME"));
		tipoCambioEntity.setFechaCrea(todaysDate.toString());
		
		
		return Single.create(x -> {
			tipoCambioRepository.save(tipoCambioEntity);
			x.onSuccess(null);
		});
		
	}

	@Override
	public Single<Void> actualizar(TipoCambioModel tipoCambio) {
		LocalDate todaysDate = LocalDate.now();				
		return Single.create(x -> {
			tipoCambioRepository.actualizar(tipoCambio.getMonOrigen(), tipoCambio.getMonDestino(), tipoCambio.getMontoTCambio(), System.getenv("USERNAME"), todaysDate.toString());
			x.onSuccess(null);
		});
	}

	@Override
	public Single<TipoCambioModel> consultarCambio(String monOrigen, String monDestino, Double montoAntesCambio) {
		
		return Single.create(x -> {
			TipoCambioModel tipoCambioModel = new TipoCambioModel();
			List<TipoCambioEntity> rptaCambio = tipoCambioRepository.consultarCambio(monOrigen, monDestino);
			System.out.println("rptaCambio: " + rptaCambio.get(0).getMontoTCambio());
			TipoCambioEntity entity = rptaCambio.get(0);
			double montoDespuesCambio = montoAntesCambio / entity.getMontoTCambio();			
			tipoCambioModel.setMontoDespuesCambio(montoDespuesCambio);
			tipoCambioModel.setMontoTCambio(entity.getMontoTCambio());
			tipoCambioModel.setMontoAntesCambio(montoAntesCambio);
			tipoCambioModel.setMonOrigen(monOrigen);
			tipoCambioModel.setMonDestino(monDestino);
			
			x.onSuccess(tipoCambioModel);
	    });
	}

}
