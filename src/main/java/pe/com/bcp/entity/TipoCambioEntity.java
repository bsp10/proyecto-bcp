package pe.com.bcp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TipoCambio")
public class TipoCambioEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public TipoCambioEntity(){}
	

	public TipoCambioEntity(String monOrigen, String monDestino, Double montoTCambio,
			Double resultadoCambio, String usuarioCrea, String fechaCrea, String usuarioAct, String fechaAct) {
		this.monOrigen = monOrigen;
		this.monDestino = monDestino;
		this.montoTCambio = montoTCambio;
		this.resultadoCambio = resultadoCambio;
		this.usuarioCrea = usuarioCrea;
		this.fechaCrea = fechaCrea;
		this.usuarioAct = usuarioAct;
		this.fechaAct = fechaAct;
	}


	@Id
	@Column(name="id")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="monOrigen", nullable = false)
	private String monOrigen;
	
	@Column(name="monDestino", nullable = false)
	private String monDestino;
	
	@Column(name="montoTCambio")
	private Double montoTCambio;
	
	private Double resultadoCambio;
	
	@Column(name="usuarioCrea")
	private String usuarioCrea;
	
	@Column(name="fechaCrea")
	private String fechaCrea;
	
	@Column(name="usuarioAct")
	private String usuarioAct;
	
	@Column(name="fechaAct")
	private String fechaAct;

	public Double getResultadoCambio() {
		return resultadoCambio;
	}


	public void setResultadoCambio(Double resultadoCambio) {
		this.resultadoCambio = resultadoCambio;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMonOrigen() {
		return monOrigen;
	}

	public void setMonOrigen(String monOrigen) {
		this.monOrigen = monOrigen;
	}

	public String getMonDestino() {
		return monDestino;
	}

	public void setMonDestino(String monDestino) {
		this.monDestino = monDestino;
	}

	public Double getMontoTCambio() {
		return montoTCambio;
	}

	public void setMontoTCambio(Double montoTCambio) {
		this.montoTCambio = montoTCambio;
	}

	public String getUsuarioCrea() {
		return usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getFechaCrea() {
		return fechaCrea;
	}

	public void setFechaCrea(String fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public String getUsuarioAct() {
		return usuarioAct;
	}

	public void setUsuarioAct(String usuarioAct) {
		this.usuarioAct = usuarioAct;
	}

	public String getFechaAct() {
		return fechaAct;
	}

	public void setFechaAct(String fechaAct) {
		this.fechaAct = fechaAct;
	}
	
	
}
