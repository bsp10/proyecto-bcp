package pe.com.bcp.service;

import pe.com.bcp.model.TipoCambioModel;
import rx.Single;

public interface TipoCambioService {

	Single<Void> save(TipoCambioModel tipoCambio);
	Single<Void> actualizar(TipoCambioModel tipoCambio);
	Single<TipoCambioModel> consultarCambio(String monOrigen, String monDestino, Double montoAntesCambio);
}
