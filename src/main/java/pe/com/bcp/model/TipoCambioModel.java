package pe.com.bcp.model;

public class TipoCambioModel {
	
	private String monOrigen;
	private String monDestino;
	private Double montoTCambio;
	private Double montoAntesCambio;
	private Double montoDespuesCambio;
	public String getMonOrigen() {
		return monOrigen;
	}
	public void setMonOrigen(String monOrigen) {
		this.monOrigen = monOrigen;
	}
	public String getMonDestino() {
		return monDestino;
	}
	public void setMonDestino(String monDestino) {
		this.monDestino = monDestino;
	}
	public Double getMontoTCambio() {
		return montoTCambio;
	}
	public void setMontoTCambio(Double montoTCambio) {
		this.montoTCambio = montoTCambio;
	}
	
	public Double getMontoAntesCambio() {
		return montoAntesCambio;
	}
	public void setMontoAntesCambio(Double montoAntesCambio) {
		this.montoAntesCambio = montoAntesCambio;
	}
	public Double getMontoDespuesCambio() {
		return montoDespuesCambio;
	}
	public void setMontoDespuesCambio(Double montoDespuesCambio) {
		this.montoDespuesCambio = montoDespuesCambio;
	}

}

