package pe.com.bcp.model;

public class RespuestaOK {
	
	private String mensaje;

	public RespuestaOK(String mensaje) {
		super();
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
